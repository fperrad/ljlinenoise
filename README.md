
ljlinenoise : Line editing in pure LuaJIT
=========================================

Introduction
------------

ljlinenoise is a pure [LuaJIT](https://luajit.org/) port
of [linenoise](https://github.com/antirez/linenoise),
a small alternative to readline and libedit.

ljlinenoise is based on [ljsyscall](https://github.com/justincormack/ljsyscall).

ljlinenoise is compatible with [lua-linenoise](https://github.com/hoelzro/lua-linenoise).

Links
-----

The homepage is at <https://fperrad.frama.io/ljlinenoise>
and the sources are hosted at <https://framagit.org/fperrad/ljlinenoise>.

Copyright and License
---------------------

Copyright (c) 2013-2018 Francois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.

