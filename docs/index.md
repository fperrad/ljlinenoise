
# ljlinenoise

---

## Overview

ljlinenoise is a pure [LuaJIT](https://luajit.org/)
port of [linenoise](https://github.com/antirez/linenoise),
a small alternative to readline and libedit.

ljlinenoise is based on
[ljsyscall](https://github.com/justincormack/ljsyscall).

ljlinenoise is compatible with
[lua-linenoise](https://github.com/hoelzro/lua-linenoise).

## Status

ljlinenoise is in beta stage.

It's developed for LuaJIT 2.x.

## Download

ljlinenoise source can be downloaded from
[Framagit](https://framagit.org/fperrad/ljlinenoise).

## Installation

The easiest way to install ljlinenoise is to use LuaRocks:

```sh
luarocks install ljlinenoise
```

or manually, with:

```sh
make install
```

## Copyright and License

Copyright &copy; 2013-2018 Fran&ccedil;ois Perrad

This library is licensed under the terms of the MIT/X11 license,
like Lua itself.
