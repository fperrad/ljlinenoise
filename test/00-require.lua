#!/usr/bin/env luajit

require 'Test.Assertion'

plan(21)

if not require_ok 'linenoise' then
    BAIL_OUT "no lib"
end

local m = require 'linenoise'
is_table( m )
equals( m, package.loaded['linenoise'] )
matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'editing', "_DESCRIPTION" )
is_string( m._VERSION, "_VERSION" )
matches( m._VERSION, '^%d%.%d%.%d$' )

is_function( m.linenoise, 'linenoise' )
is_function( m.historyadd, 'historyadd' )
is_function( m.historysetmaxlen, 'historysetmaxlen' )
is_function( m.historysave, 'historysave' )
is_function( m.historyload, 'historyload' )
is_function( m.clearscreen, 'clearscreen' )
is_function( m.setcompletion, 'setcompletion' )
is_function( m.addcompletion, 'addcompletion' )
is_function( m.addhistory, 'addhistory' )
is_function( m.sethistorymaxlen, 'sethistorymaxlen' )
is_function( m.savehistory, 'savehistory' )
is_function( m.loadhistory, 'loadhistory' )
is_function( m.line, 'line' )
is_function( m.lines, 'lines' )
