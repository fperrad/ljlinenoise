#!/usr/bin/env luajit

require 'Test.Assertion'

if not jit then
    skip_all 'not LuaJIT'
end

plan(7)

local S = require 'syscall'

truthy( S.stdin:isatty(), "isatty" )

local termios = S.stdin:tcgetattr()
truthy( termios, "tcgetattr" )
truthy( S.stdin:tcsetattr('FLUSH', termios), "tcsetattr" )
truthy( termios:makeraw(), "makeraw" )


local ws = S.ioctl(S.stdin, 'TIOCGWINSZ')
truthy( ws, "winsize" )
is_number( ws.ws_col, 'ws_col' )
truthy( ws.ws_col >= 0 )
