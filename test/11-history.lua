#!/usr/bin/env luajit

local l = require 'linenoise'

require 'Test.Assertion'

plan(36)

local h1 = l.new_history()
is_table( h1, "history" )
equals( h1.max_len, 100, "default max" )
equals( #h1, 0, "empty" )

truthy( h1:setmaxlen(6), "setmaxlen" )
equals( h1.max_len, 6, "setmaxlen" )

falsy( h1:setmaxlen(0), "setmaxlen(0)" )
equals( h1.max_len, 6 )
falsy( h1:setmaxlen(-1), "setmaxlen(-1)" )
equals( h1.max_len, 6 )

h1:add'line1'
equals( #h1, 1, "1 line" )
equals( h1[1], 'line1', "line1" )

h1:add'line2'
h1:add'line3'
equals( #h1, 3, "3 lines" )
equals( h1[1], 'line1', "line1" )
equals( h1[2], 'line2', "line2" )
equals( h1[3], 'line3', "line3" )

h1:add'line4'
h1:add'line5'
h1:add'line6'
h1:add'line7'
h1:add'line8'
equals( #h1, 6, "max lines" )
equals( h1[1], 'line3', "line3" )
equals( h1[6], 'line8', "line8" )

h1:setmaxlen(4)
equals( #h1, 4, "max lines -> 4" )
equals( h1[1], 'line5', "line5" )
equals( h1[4], 'line8', "line8" )

h1:add'line9'
equals( #h1, 4, "max lines" )
equals( h1[1], 'line6', "line6" )
equals( h1[4], 'line9', "line9" )

h1:setmaxlen(8)
equals( h1.max_len, 8, "max lines -> 8" )
equals( #h1, 4 )

truthy( h1:save'test.txt', "save" )

h1:clean()
equals( #h1, 0, "free" )

local h2 = l.new_history()
truthy( h2:load'test.txt', "load" )
equals( #h2, 4, "restored" )
equals( h2[1], 'line6', "line6" )
equals( h2[4], 'line9', "line9" )

h2:updatelast'line0'
equals( #h2, 4, "updatelast" )
equals( h2[4], 'line0' )

local r, msg = h2:load'no_file'
falsy( r, "load no file" )
matches( msg, "no_file" )
