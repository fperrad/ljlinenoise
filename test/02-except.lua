#!/usr/bin/env luajit

local l = require 'linenoise'

require 'Test.Assertion'

plan(7)

error_matches( function () assert(l.historysetmaxlen(true)) end,
        "bad argument #1 to setmaxlen %(number expected" )

error_matches( function () assert(l.historysetmaxlen(-1)) end,
        "bad argument #1 to setmaxlen" )

error_matches( function () assert(l.setcompletion(true)) end,
        "bad argument #1 to setcompletion %(function expected" )

error_matches( function () l.addcompletion(true, "text") end,
        "bad argument" )

error_matches( function () l.historysave(true) end,
        "bad argument" )

error_matches( function () l.historyload(true) end,
        "bad argument" )

error_matches( function () assert(l.historyload('no_file')) end,
        "no_file" )

